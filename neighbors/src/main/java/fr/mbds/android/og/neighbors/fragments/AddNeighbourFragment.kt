package fr.mbds.android.og.neighbors.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.core.view.get
import androidx.core.view.size
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputLayout
import fr.mbds.android.og.neighbors.NavigationListener
import fr.mbds.android.og.neighbors.R
import fr.mbds.android.og.neighbors.adapters.AddNeighborHandler
import fr.mbds.android.og.neighbors.data.NeighborRepository
import fr.mbds.android.og.neighbors.models.Neighbor

class AddNeighbourFragment : Fragment(), AddNeighborHandler {
    private lateinit var backToList: Button
    private lateinit var register: Button
    private lateinit var imageNeighbor: ImageView
    private lateinit var image: TextInputLayout
    private lateinit var name: TextInputLayout
    private lateinit var phone: TextInputLayout
    private lateinit var webSite: TextInputLayout
    private lateinit var adress: TextInputLayout
    private lateinit var aboutMe: TextInputLayout
    private final val regexPhoneFr: Regex = Regex("^(0)(6|7)[0-9]{8}$")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.add_neighbor, container, false)
        backToList = view.findViewById(R.id.backToList)
        register = view.findViewById(R.id.buttonAdd)
        imageNeighbor = view.findViewById(R.id.imageView)
        image = view.findViewById(R.id.textFieldImage)
        name = view.findViewById(R.id.textFieldNom)
        phone = view.findViewById(R.id.textFieldTelephone)
        webSite = view.findViewById(R.id.textFieldWebsite)
        adress = view.findViewById(R.id.textFieldAdresse)
        aboutMe = view.findViewById(R.id.textFieldApropos)
        register.isEnabled = false
        register.isClickable = false

        this.control()

        backToList.setOnClickListener {
            (activity as? NavigationListener)?.let {
                it.showFragment(ListNeighborsFragment())
            }
        }

        (activity as? NavigationListener)?.let {
            it.updateTitle("Nouveau voisin")
        }

        register.setOnClickListener {
            // Setting new Neighbor
            val id = getNeighborListSize().toLong() + 1
            val image = image?.getEditText()?.getText().toString()
            val name = name?.getEditText()?.getText().toString()
            val phone = phone?.getEditText()?.getText().toString()
            val webSite = webSite?.getEditText()?.getText().toString()
            val adresse = adress?.getEditText()?.getText().toString()
            val aboutMe = aboutMe?.getEditText()?.getText().toString()
            val newNeighbor = Neighbor(
                id = id,
                name = name,
                avatarUrl = image,
                address = adresse,
                phoneNumber = phone,
                aboutMe = aboutMe,
                favorite = false,
                webSite = webSite
            )

            this.addNeibour(newNeighbor)
        }
        return view
    }

    override fun addNeibour(neighbor: Neighbor) {
        NeighborRepository.getInstance().addNeighbour(neighbor)
        Toast.makeText(context, "Neighbor created", Toast.LENGTH_LONG).show()
        (activity as? NavigationListener)?.let {
            it.showFragment(ListNeighborsFragment())
        }
    }

    private fun control() {
        this.controlImage()
        this.controlName()
        this.controlPhone()
        this.controlWebsite()
        this.controlAdress()
        this.controlAboutMe()
    }

    private fun controlImage() {
        image.getEditText()?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.isEmpty() == true) {
                    image.error = "Image is required"
                    image.isErrorEnabled = true
                } else if (!Patterns.WEB_URL.matcher(p0).matches()) {
                    image.error = "Image URL dismatch"
                    image.isErrorEnabled = true
                } else {
                    image.isErrorEnabled = false
                }
            }

            override fun afterTextChanged(p0: Editable?) {
                validateFields()
                /*val imageUrl = URL(image.getEditText()?.text.toString()).openStream()
                val imageBitmaped: Bitmap? = BitmapFactory.decodeStream(imageUrl)
                imageNeighbor.setImageBitmap(imageBitmaped)*/
            }
        })
    }

    private fun controlName() {
        name.getEditText()?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.isEmpty() == true) {
                    name.error = "Name is required"
                    name.isErrorEnabled = true
                } else {
                    name.isErrorEnabled = false
                }
            }

            override fun afterTextChanged(p0: Editable?) {
                validateFields()
            }
        })
    }

    private fun controlPhone() {
        phone.getEditText()?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.isEmpty() == true) {
                    phone.error = "Phone is required"
                    phone.isErrorEnabled = true
                } else if (!p0?.let { regexPhoneFr.containsMatchIn(it) }!!) {
                    phone.error = "Phone dismatch"
                    phone.isErrorEnabled = true
                } else {
                    phone.isErrorEnabled = false
                }
            }

            override fun afterTextChanged(p0: Editable?) {
                validateFields()
            }
        })
    }

    private fun controlWebsite() {
        webSite.getEditText()?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.isEmpty() == true) {
                    webSite.error = "WebSite is required"
                    webSite.isErrorEnabled = true
                } else if (!Patterns.WEB_URL.matcher(p0).matches()) {
                    webSite.error = "WebSite URL dismatch"
                    webSite.isErrorEnabled = true
                } else {
                    webSite.isErrorEnabled = false
                }
            }

            override fun afterTextChanged(p0: Editable?) {
                validateFields()
            }
        })
    }

    private fun controlAdress() {
        adress.getEditText()?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.isEmpty() == true) {
                    adress.error = "Adresse is required"
                    adress.isErrorEnabled = true
                } else {
                    adress.isErrorEnabled = false
                }
            }

            override fun afterTextChanged(p0: Editable?) {
                validateFields()
            }
        })
    }

    private fun controlAboutMe() {
        aboutMe.getEditText()?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.isEmpty() == true) {
                    aboutMe.error = "About me is required"
                    aboutMe.isErrorEnabled = true
                } else if (p0.toString().length > 30) {
                    aboutMe.error = "30 caracteres maximum"
                    aboutMe.isErrorEnabled = true
                } else {
                    aboutMe.isErrorEnabled = false
                }
            }

            override fun afterTextChanged(p0: Editable?) {
                validateFields()
            }
        })
    }

    private fun getNeighborListSize(): Int {
        return NeighborRepository.getInstance().getNeighbours().size
    }

    private fun validateFields() {

        if (image.getEditText()?.getText().toString().length > 0 &&
            !image.isErrorEnabled &&
            name.getEditText()?.getText().toString().length > 0 &&
            !name.isErrorEnabled &&
            phone.getEditText()?.getText().toString().length > 0 &&
            !phone.isErrorEnabled &&
            webSite.getEditText()?.getText().toString().length > 0 &&
            !webSite.isErrorEnabled &&
            adress.getEditText()?.getText().toString().length > 0 &&
            !adress.isErrorEnabled &&
            aboutMe.getEditText()?.getText().toString().length > 0 &&
            !aboutMe.isErrorEnabled
        ) {
            register.setEnabled(true)
        } else {
            register.setEnabled(false)
        }
    }
}
