package fr.mbds.android.og.neighbors.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import fr.mbds.android.og.neighbors.R
import fr.mbds.android.og.neighbors.models.Neighbor

class ListNeighborsAdapter(
    items: List<Neighbor>,
    private val iHandler: ListNeighborHandler
) : RecyclerView.Adapter<ListNeighborsAdapter.ViewHolder>() {
    private val mNeighbours: List<Neighbor> = items
    private lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.neighbor_item, parent, false)

        context = parent.context
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val neighbour: Neighbor = mNeighbours[position]
        // Display Neighbour Name
        holder.mNeighbourName.text = neighbour.name

        Glide.with(context)
            .load(neighbour.avatarUrl)
            .apply(RequestOptions.circleCropTransform())
            .placeholder(R.drawable.ic_baseline_person_24)
            .error(R.drawable.ic_baseline_person_24)
            .skipMemoryCache(false)
            .into(holder.mNeighbourAvatar)

        holder.mDeleteButton.setOnClickListener {
            iHandler.onDeleteNeibour(neighbour)
        }
    }

    override fun getItemCount(): Int {
        return mNeighbours.size
    }

    class ViewHolder(view: View) :
        RecyclerView.ViewHolder(view) {
        val mNeighbourAvatar: ImageView = view.findViewById(R.id.item_list_avatar)
        val mNeighbourName: TextView = view.findViewById(R.id.item_list_name)
        val mDeleteButton: ImageButton = view.findViewById(R.id.item_list_delete_button)

        init {
            // Enable click on item
        }
    }
}
