package fr.mbds.android.og.neighbors.adapters

import fr.mbds.android.og.neighbors.models.Neighbor

interface ListNeighborHandler {
    fun onDeleteNeibour(neighbor: Neighbor)
}
