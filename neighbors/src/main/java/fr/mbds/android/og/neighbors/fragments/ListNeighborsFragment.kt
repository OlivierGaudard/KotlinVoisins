package fr.mbds.android.og.neighbors.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import fr.mbds.android.og.neighbors.NavigationListener
import fr.mbds.android.og.neighbors.R
import fr.mbds.android.og.neighbors.adapters.ListNeighborHandler
import fr.mbds.android.og.neighbors.adapters.ListNeighborsAdapter
import fr.mbds.android.og.neighbors.data.NeighborRepository
import fr.mbds.android.og.neighbors.models.Neighbor

class ListNeighborsFragment : Fragment(), ListNeighborHandler {
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.list_neighbors_fragment, container, false)
        recyclerView = view.findViewById(R.id.neighbors_list)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
        )
        val addNeighbor = view.findViewById(R.id.addNeighbor) as FloatingActionButton
        addNeighbor.setOnClickListener {
            (activity as? NavigationListener)?.let {
                it.showFragment(AddNeighbourFragment())
            }
        }
        (activity as? NavigationListener)?.let {
            it.updateTitle("Liste des voisins")
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.getNeighbourList()
    }

    override fun onDeleteNeibour(neighbor: Neighbor) {
        val alertDialog = AlertDialog.Builder(context)
        alertDialog
            .setTitle("Confirmation")
            .setMessage("Voulez-vous supprimer ce voisin ?")
            .setPositiveButton("Oui") {
                dialog, id ->
                NeighborRepository.getInstance().deleteNeighbour(neighbor)
                this.getNeighbourList()
            }
            .setNegativeButton("Non") {
                dialog, id ->
                dialog.cancel()
            }
        val createdAlertDialog = alertDialog.create()
        createdAlertDialog.show()
    }

    fun getNeighbourList() {
        val neighbors = NeighborRepository.getInstance().getNeighbours()
        val adapter = ListNeighborsAdapter(neighbors, this)
        recyclerView.adapter = adapter
    }
}
